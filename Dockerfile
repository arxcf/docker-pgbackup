FROM alpine:3.10.1
RUN apk add --update \
    bash \
    postgresql-client \
    curl \
    jq \
    python-dev \
    py-pip \
    py-setuptools \
    ca-certificates \
    gcc \
    libffi-dev \
    openssl-dev \
    musl-dev \
    linux-headers \
    && pip install --upgrade --no-cache-dir pip setuptools python-openstackclient python-novaclient python-swiftclient \
    && apk del gcc musl-dev linux-headers libffi-dev \
    && rm -rf /var/cache/apk/*
CMD ["/bin/bash"]